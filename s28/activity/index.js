db.hotel.insertOne({
	"name": "single",
	"accomodates": "3",
	"price": 1000.00,
	"description": "A simple room with all the basic necesitties",
	"roomsAvailable": 10,
	"isAvailable": false
});

db.hotel.insertMany([
	{
		"name": "queen",
		"accomodates": "4",
		"price": 4000.00,
		"description": "A room with a queen sized bed perfect for a simple getaway",
		"roomsAvailable": 15,
		"isAvailable": false,


	},
	{
		"name": "double",
		"accomodates": "3",
		"price": 2000.00,
		"description": "A room fit for a small family going on a vacation",
		"roomsAvailable": 5,
		"isAvailable": false

	}


]);

db.hotel.find({"name": "double"});

db.hotel.updateOne(
		{
			"name": "queen"
		},
		{
			$set: {
				"roomsAvailable" : 0
			}
		}
	);

db.hotel.find({"name":"queen"});



db.hotel.deleteMany({
	"roomsAvailable": 0
})