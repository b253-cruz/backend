let users = ["Dwayne Johnson","Steve Austin","Kurt Angle","Dave Bautista"];

console.log("Original Array:")
console.log(users);

function addItem(item){
    users[users.length] = item;
}

addItem("John Cena");
console.log(users)

function getItemByIndex(index){
    return users[index]
}

let itemFound = getItemByIndex(2);
console.log(itemFound);


function deleteItem(){
    let deleted = users[users.length-1]
    users.length--;
    return deleted;
}

let deletedItem = deleteItem();
console.log(deletedItem);
console.log(users);


function updateItemByIndex(update,index){
    users[index] = update;
}

updateItemByIndex("Triple H",3);
console.log(users);



function deleteAll(){
    users.length = 0;
    // users = [];
}

deleteAll();
console.log(users)

function isEmpty(){
    if(users.length > 0){
        return false
    } else {
        return true
    }
}
let isUsersEmpty = isEmpty();
console.log(isUsersEmpty);

function getIndexByItem(item){

    for(let index = 0; index < users.length; index++){

        if(item === users[index]){
            return index;
        } 
    }

    return -1

}

let indexOfItem = getIndexByItem("Dwayne Johnson");

console.log(indexOfItem);


try {
   module.exports = {
       users,
       addItem,
       getItemByIndex,
       deleteItem,
       updateItemByIndex,
       deleteAll,
       isEmpty,
       getIndexByItem
   } 
} catch (err) {

}
