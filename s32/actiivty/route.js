let http = require("http");

http.createServer((request, response)=>{

	if(request.url == "/welcome" && request.method == "GET"){

		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Welcome to booking System")
	}else if(request.url == "/profile" && request.method == "GET"){
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Welcome to profile")
	}else if(request.url == "/courses" && request.method == "GET"){
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Here's our courses available")
	}else if(request.url == "/addCourse" && request.method == "POST"){
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Add course to our resources")
	}else if(request.url == "/updateCourse" && request.method == "PUT"){
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Update a course to our resourses")
	}else if(request.url == "/archiveCourse" && request.method == "DELETE"){
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Archive courses to our resources")
	}

}).listen(4000);

console.log("Running at localserver:4000");