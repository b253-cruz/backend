/*
	//Note: strictly follow the variable names and function names.

	1. Create a function named printUserInfo() which is able to display a user's to fullname, age, location and other information.
		-invoke the function to display information similar to the expected output in the console.
		-check your spelling


*/

console.log("Hello!");

function printUserInfo(){


	let name = "Hello, I am John Doe.";
	let age = "I am 27 years old";
	let address = "I live in 123 street, Quezon City City";
	let cat = "I have a cat named Joe."
	let dog = "I have a dog named Danny."
	console.log("printUserInfo")
	console.log(name);
	console.log(age);
	console.log(address);
	console.log(cat);
	console.log(dog);

};

printUserInfo();

//first function here:

/*
	2. Create a function named printFiveBands which is able to display 5 bands/musical artists.
		-invoke the function to display information similar to the expected output in the console.
		-check your spelling
*/

function printFiveBands (){
	let band1 = "The Beatles";
	let band2 = "Taylor Swift";
	let band3 = "The Eagles";
	let band4 = "Rivermaya";
	let band5 = "Eraserheads";

	console.log("printFiveBands");
	console.log(band1);
	console.log(band2);
	console.log(band3);
	console.log(band4);
	console.log(band5);
};


printFiveBands();


	//second function here:
/*
	3. Create a function named printFiveMovies which is able to display the name of 5 movies.
		-invoke the function to display information similar to the expected output in the console.
		-check your spelling
	
*/

function printFiveMovies(){

	let movie1 = "Lion King";
	let movie2 = "Howl's Moving Castles";
	let movie3 = "Meet the Robinsons";
	let movie4 = "School of Rock";
	let movie5 = "Sprited Away";

	console.log("printFiveMovies")
	console.log(movie1);
	console.log(movie2);
	console.log(movie3);
	console.log(movie4);
	console.log(movie5);
}

printFiveMovies();
	
	//third function here:

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
		-check your spelling

		-invoke the function to display information similar to the expected output in the console.
*/

function printFriends(){
	let friend1 = "Eugene"; 
	let friend2 = "Dennis"; 
	let friend3 = "Vincent";

	console.log("These are my friends:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3);  

};

printFriends();




//Do not modify
//For exporting to test.js
try{
	module.exports = {
		printUserInfo,
		printFiveBands,
		printFiveMovies,
		printFriends
	}
} catch(err){

}