const User = require("../models/Users");
const auth = require("../auth");
const Product = require("../models/Products");
const bcrypt = require("bcrypt");


module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0){
			return true;
		}else {
			return false;
		}
	}).catch(err => err);
};


module.exports.registerUser = (reqBody) => {
	let newUser = new User ({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email: reqBody.email,
		password : bcrypt.hashSync(reqBody.password, 10)
	});

	return newUser.save().then(user => {
		if(user){
			return true
		}else {
			return false
		}
	}).catch(err => err)
};


module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result => {
		if(result === null){
				return false;
		}else{			
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if(isPasswordCorrect){			
				return  { access : auth.createAccessToken(result)}
			}else{
				return false;
				}
		}
	}).catch(err => err);
};


/*module.exports.checkOutProduct = async (data, reqBody) => {
  let isUserUpdated = await User.findOne(data.userId)
    .then(user => {
      user.orderedProduct.products.push ({ productName : reqBody.productName });
      return user.save().then(user => true)
      .catch(err => false);
    });
  let isProductUpdated = await Product.findOne(reqBody.productId)
    .then(product => {
      product.userOrders.push({ userId : data.userId });

      return product.save().then(product => true)
      .catch(err => false);
    });

  if(isUserUpdated && isProductUpdated){
    return true;
  }else{
    return false;
  }
}*/

module.exports.checkout = (data, body) => {
    const id = data.userId;
    console.log(data);

    return User.findById(id).then(user => {
        user.orderedProduct.push({ products : body});
        console.log(user.orderedProduct[0].products);
        return user.save();
    }).catch(err => {
        console.error(err);
        throw err;
    });
};

module.exports.getProfile = (id) => {
	return User.findById(id)
	.then(user => {

		console.log(user,id)
		user.password = "";
		return user;
	}) .catch(err => err);
};













