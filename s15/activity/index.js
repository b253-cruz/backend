console.log("Hello World");

let firstName = ("First Name: Jimbo");
console.log(firstName);

let lastName = ("Last Name: Cruz");
console.log(lastName);

let age = ("Age: 30");
console.log(age)
;
let hobbies = ["Reading", "Basketball", "Valorant"];
console.log(hobbies);

let workAddress = {
	houseNumber: "123",
	street: "Concepcion",
	city: "Marikina",
	state: "Metro Manila",
}
console.log("work address: ")
console.log(workAddress)

/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:




/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	

	let fullName = "Steve Rogers";
	console.log("My full name is: " + fullName);

	let myAge = ("40");
	console.log("My current age is: " + myAge);
	
	let friends = ["Tony","Bruce","Thor","Natasha","Clint","Nick"];
	console.log("My Friends are: ")
	console.log(friends);

	let profile = {

		userName: "captain_america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false,

	}
	console.log("My Full Profile: ")
	console.log(profile);

	let buckyName = "Bucky Barnes";
	console.log("My bestfriend is: " + buckyName);

	const lastLocation = "Arctic Ocean";
	// lastLocation = "Atlantic Ocean";
	console.log("I was found frozen in: " + lastLocation);



//Do not modify
    //For exporting to test.js
   /* try{
        module.exports = {
            firstName, lastName, age, hobbies, workAddress,
            fullName, currentAge, friends, profile, fullName2, lastLocation
        }
    } catch(err){
    }*/