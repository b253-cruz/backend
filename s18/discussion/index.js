// console.log("Hello!")
//Functions
	
		//Parameters and Arguments

			// Functions in javascript are lines/blocks of codes that tell our device/application to perform a certain task when called/invoked
			// Functions are mostly created to create complicated tasks to run several lines of code in succession
			// They are also used to prevent repeating lines/blocks of codes that perform the same task/function

			//We also learned in the previous session that we can gather data from user input using a prompt() window.


// function printInput (){
// 	let nickname = prompt("Enter your nickame");
// 	console.log("Hi, " + nickname);
// };

// printInput();

function printName(name){
	console.log("My name is " + name);
};

printName("Jimbo Cruz");
printName("Enma Ai");
printName("Roronoa Zoro");

// Variables can also be passed as an argument
let sampleName = "Yui";
printName(sampleName);


function isDivisibleBy8(number){
	let remainder = number % 8 === 0;
	console.log(remainder)
}

isDivisibleBy8(68);
isDivisibleBy8(24);


// Functions as Argument
function argumentFucntion(){
	console.log("This function was passed as an argument before the message is printer.");
};

function invokeFunction(argumentFucntion) {
	argumentFucntion();
};

invokeFunction(argumentFucntion);

// Multipe Parameters

function createFullName(givenName, middleName, surName){

	console.log(givenName + " " +middleName +" " + surName);
};

createFullName("Juan","Dela","Cruz" );
createFullName("Juan", "Dela") // if argument is lesser than the parameters, the last argument that is not included will return as undefined
createFullName("Juan", "Dela", "Cruz", "Hello") // if the argument is more than the parameters the exceeding argument will not display

let firstName = "John"
let middleName = "Doe"
let lastName = "Smith"

createFullName(firstName, middleName, lastName)

//The order of the argument is the same to the order of the parameters. The first argument will be stored in the first parameter, second argument will be stored in the second parameter and so on.

// The Return Statement
// The "return" statement allows us to output a value from a function to be passed to the line/block of code that invoked/called the function.

function returnFullName(firstName, middleName, lastName){

	return firstName + " " + middleName + " " + lastName;

	console.log("Cute Ko");
	// Notice that the console.log() after the return is no longer printed in the console that is because ideally any line/block of code that comes after the return statement is ignored because it ends the function execution.
};

let completeName = returnFullName("Lucy", " ", "Pevensie");

console.log(completeName);

console.log(returnFullName(firstName, middleName, lastName));

function returnAddress(city, country){
	let fullAddress = city + ", " + country;
	return fullAddress;
};

let address = returnAddress("Shire", "Middle Earth");
console.log(address);

function printPlayerInfo(username, level, jobClass){

	console.log("Username: " + username);
	console.log("Level: " + level);
	console.log("Job: " + jobClass);
};

// 	return "Username: " + username + "\n" + "Level: " + level "\n" + "Job: " + jobClass;
// };

let user1 = printPlayerInfo("Kardel", 99, "Sniper");

console.log(user1);

//returns undefined because printPlayerInfo returns nothing. It only console.logs the details. 

//You cannot save any value from printPlayerInfo() because it does not return anything