console.log("Hello!")


function addNum(num1, num2){

	let sum = num1 + num2;
console.log("Displayed sum of 5 and 15:");
console.log(sum)
};
addNum(5,15);

function subNum(num1, num2){

	let difference = num1 - num2;
console.log("Displayed difference of 5 and 20:");
console.log(difference)
};
subNum(20,5);


function multiplyNum(num1, num2){
	let multiply = num1 * num2;
	return multiply;

};

let product = multiplyNum(50,10);
console.log("The product of 50 and 10:");
console.log(product)


function divideNum(num1, num2){
	let divide = num1 / num2;

	return divide;

};

let quotient = divideNum(50,10);
console.log("The quotient of 50 and 10:");
console.log(quotient);


function getCircleArea(num1, num2){
	let area = num1 * num2**2

	return area;
};

let circleArea = getCircleArea(3.1416, 15);

console.log("The result of getting the area of a circle with 15 radius:");
console.log(circleArea);

function getAverage(num1, num2, num3, num4, num5){

	let average = (num1 + num2 + num3 + num4)/num5;

	return average;
};

let averageVar = getAverage(20,40,60,80,4)
console.log("The average of 20,40,60 and 80");
console.log(averageVar);

function checkIfPassed(num1,num2){
	

	let isPassed = (num1 / num2 )* 100 > 75;
	return isPassed

	
};
let isPassingScore = checkIfPassed(38,50)
console.log("Is 38/50 a passing score?")
console.log(isPassingScore);











//Do not modify
//For exporting to test.js
try {
	module.exports = {
		addNum,subNum,multiplyNum,divideNum,getCircleArea,getAverage,checkIfPassed
	}
} catch (err) {

}