console.log("Hello World");

let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu","Charizard","Squirtle", "Bulbasaur"],
	friends: { 
		hoen: ["May","Max"],
		kanto:["Brock", "Misty"]
	},
	talk:function(){
		console.log("Pikachu I choose you!")
	}
};

console.log(trainer)

console.log("Result of dot notation:")
console.log(trainer.name);

console.log("Result of square bracket notation:");
console.log(trainer["pokemon"]);

console.log("Result of talk method:")
trainer.talk();


function Pokemon(name, level){
	this.name = name;
	this.level = level;
	this.health = level * 2;
	this.attack = level;

	this.tackle = function(target){
		console.log(this.name + " tackled" + target.name);
		console.log( target.name + "'s health is now reduced to "+ damage);
	},
	this.faint = function(){
		console.log(this.name + "fainted")
	
	}

};


let geodude = new Pokemon("Geodude", 8);
let mewTwo = new Pokemon("MewTwo", 100);
let pikachu = new Pokemon("Pikachu", 12 );
console.log(pikachu);
console.log(geodude);
console.log(mewTwo);
geodude.tackle(pikachu)



//Follow the property names and spelling given in the google slide instructions.

// Create an object called trainer using object literals

// Initialize/add the given object properties and methods

// Properties

// Methods

// Check if all properties and methods were properly added


// Access object properties using dot notation

// Access object properties using square bracket notation

// Access the trainer "talk" method


// Create a constructor function called Pokemon for creating a pokemon


// Create/instantiate a new pokemon


// Create/instantiate a new pokemon


// Create/instantiate a new pokemon


// Invoke the tackle method and target a different object


// Invoke the tackle method and target a different object








//Do not modify
//For exporting to test.js
try{
	module.exports = {
		trainer,
		Pokemon 
	}
} catch(err) {

}