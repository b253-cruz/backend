/*
	OBJECTS
	 - is a data type that is used to represent real world objects. It is also a collection of related data and/or functionalities
*/

	// Object literals
	/*
		- one of the methods in creating objects

		Syntax:
			let objectName = {
				keyA/propertyA: valueA,
				keyB/propertyB: ValueB
			}
	*/

let cellphone = {
	name: "Nokia 3210",
	manufactureDate: 1999
};

console.log("Result from creating object using Object Literals: ")
console.log(cellphone);
console.log(typeof cellphone);

// Creating Objects Using a Constructor Function(Object Constructor)
/*
	Creates a reusable function to create several objects that have the same data structure. This is useful for creating multiple instances/copies of an object.

	Note: when creatign an object using constructor function the naming convention will be on PascalCase

	Syntax:
		function ObjectName(param_valueA,param_valueB){
			this.keyA = valueA,
			this.keyB = valueB
		}

		let variableName = new function ObjectName(args_valueA,args_valueB)

		console.log(variableName)

		Note: Do not forget thet "new" keyword or reserved word when creating a new object
*/


// function declaration for object(constructor function/object constructor)
function Laptop(brand,manufactureDate){
	this.brand = brand,
	this.manufactureDate = manufactureDate
};

// function invocation with arguments
let laptopJanie = new Laptop("Lenovo", 2008);
console.log("Result of creating objects using constructor function:");
console.log(laptopJanie);

let laptopOwen = new Laptop("MacBook Air", [2020,2021]);
console.log("Result of creating objects using constructor function:");
console.log(laptopOwen);

let laptop = Laptop("MacBook Air", [2020,2021]);
console.log("Result of creating objects without using new keyword:");
console.log(laptop);

// Create empty objects as placeholder
let computer = {};
let myComputer = new Object();
console.log(computer);
console.log(myComputer);



// function Car(brand,manufactureDate,color){
// 	this.brand = brand,
// 	this.manufactureDate = manufactureDate,
// 	this.color = color
// }

// let carA = new Car("Toyota",2020,"Red");
// console.log(carA)

// let carB = new Car("Honda",2021,"Blue");
// console.log(carB)

// Accessing Object Properties
	//using dot notation
console.log("Result from dot notation: " + laptopJanie.brand);
console.log("Result from square bracket notation: " + laptopJanie["brand"]);

//Accessing array objects
let deviceArr = [laptopJanie, laptopOwen];
console.log(deviceArr);

//dot notation
console.log(deviceArr[0].manufactureDate);
//square bracket notation
console.log(deviceArr[0]["manufactureDate"]);

//Initializing/Adding/Deleting/Reassigning Object Properties
//CRUD OPERATIONS

//Initializing Objects
let car ={};
console.log(car);

//Adding Object Properties
//using dot notation
car.name = "Honda Civic";
console.log("Results from adding properties using dot notation");
console.log(car);


/*
	- While using the square bracket will allow access to spaces when assigning property names to make it easier to read, this also makes it so that object properties can only be accesssed using the square bracket notation
	- This also makes names of object properties to not follow commonly used naming conventions for them
*/
car["manufacture date"] = 2019;
console.log(car["manufacture date"]);
console.log(car["Manufacture date"]);
console.log(car.manufactureDate);
console.log("Results from using square brackets notation:")
console.log(car);

//Deleting Object Properties
delete car["manufacture date"];
console.log("Result from deleting properties:")
console.log(car);

//Reassigning object properties (Updates)
car.name = "Tesla";
console.log(car);

//Object Methods
/*
	This method is a function which is stored in an object property. Ther are also functions and one of the key difference that they have is that methods are functions related to a specific object.
*/

let person = {
	name: "Pedro",
	age: 25,
	talk: function(){
		console.log("Hello! Ako si " + this.name)
	}
};

console.log(person);
console.log("Result from object method")
person.talk();

person.walk = function(){
	console.log(this.name + " have walked 25 steps forward.")
};

person.walk();

let friend = {
	firstName: "Maria",
	lastName: "Dela Cruz",
	address: {
		city:"Austin,Texas",
		country:"US"
	},
	emails: ["maria@mail.com", "maria_bosxz@mail.com"],
	introduce: function(){
		console.log("Hello! my name is" +" " + this.firstName + " " + this.lastName + "." + "I live in" + " " + this.address.city + "," + " " + this.address.country + ".")
	}
};

friend.introduce();

let myPokemon = {
	name: "Charizard",
	level: 3,
	health: 100,
	attackk: 50,
	tackle:function(){
		console.log("This pokemen tackled targetPokemon");
		console.log("targetPokemon's health is now reduced to _targetPokemonHealth_")
	},
	faint:function(){
		console.log("Pokemon fainted.");
	}
}

console.log(myPokemon);
//creating an object constructor insted will help with this process
function Pokemon(name, level){
		this.name = name;
		this.level = level;
		this.health = 2 * level;
		this.attack = level;

		//Methods

		this.tackle = function(target){
			console.log(this.name + "tackled" + target.name);
			console.log(target.name + "'s health is now reduced to" + target.health);
		},
		this.faint = function(){
			console.log(this.name + "fainted");
		}
};

// Creates new instances of the "Pokemon" object each with their unique propertiese

let pikachu = new Pokemon("Pikachu", 16);
let charmander = new Pokemon("Charmander", 8);

pikachu.tackle(charmander);
pikachu.faint();